package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.BookRepository;

public class ListViewModel extends AndroidViewModel {
    private BookRepository repository;
    private LiveData<List<Book>> books;

    public ListViewModel(Application application) {
        super(application);
        repository = new BookRepository(application);
        books = repository.getAllBooks();
    }

    LiveData<List<Book>> getBooks() {
        return books;
    }

    void deleteBook(long id) {
        repository.deleteBook(id);
    }
}
