package fr.uavignon.ceri.tp2;


import android.app.Activity;
import android.app.Application;
import android.os.Build;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class RecyclerAdapter extends RecyclerView.Adapter<fr.uavignon.ceri.tp2.RecyclerAdapter.ViewHolder> {

    private List<Book> books;
    private static final String TAG = fr.uavignon.ceri.tp2.RecyclerAdapter.class.getSimpleName();
    ListViewModel viewModel;

    public void setBooks(List<Book> books) {
        this.books = books;
        notifyDataSetChanged();
    }

    public void setViewModel(ListViewModel viewModel) {
        this.viewModel = viewModel;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.itemTitle.setText(books.get(i).getTitle());
        viewHolder.itemDetail.setText(books.get(i).getAuthors());
    }

    @Override
    public int getItemCount() {
        return books == null ? 0 : books.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemTitle;
        TextView itemDetail;

        ViewHolder(View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.item_title);
            itemDetail = itemView.findViewById(R.id.item_detail);

            itemView.setOnLongClickListener(v -> {
                Snackbar.make(v, "", Snackbar.LENGTH_INDEFINITE).setAction("Supprimer", v1 -> {
                    viewModel.deleteBook(getAdapterPosition()+1);
                }).show();
                return true;
            });

            itemView.setOnClickListener(v -> {
                long position1 = books.get(getAdapterPosition()).getId();
                /* Snackbar.make(v, "Click detected on chapter " + (position+1),
                    Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

                 */
                ListFragmentDirections.ActionFirstFragmentToSecondFragment action = ListFragmentDirections.actionFirstFragmentToSecondFragment();
                action.setBookNum(position1);
                Navigation.findNavController(v).navigate(action);


            });

        }
    }

}