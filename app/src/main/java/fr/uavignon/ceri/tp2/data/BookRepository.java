package fr.uavignon.ceri.tp2.data;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static fr.uavignon.ceri.tp2.data.BookRoomDatabase.databaseWriteExecutor;

public class BookRepository {
    private LiveData<List<Book>> allBooks;
    private MutableLiveData<Book> selectedBook = new MutableLiveData<>();

    private BookDao bookDao;

    public BookRepository(Application app) {
        BookRoomDatabase db = BookRoomDatabase.getDatabase(app);
        bookDao = db.bookDao();
        allBooks = bookDao.getAllBooks();
    }

    public void updateBook(Book book) {
        databaseWriteExecutor.execute(() -> bookDao.updateBook(book));
    }

    public void insertBook(Book book) {
        databaseWriteExecutor.execute(() -> bookDao.insertBook(book));
    }

    public void deleteBook(long id) {
        databaseWriteExecutor.execute(() -> bookDao.deleteBook(id));
    }

    public void deleteAllBooks() {
        databaseWriteExecutor.execute(() -> bookDao.deleteAllBooks());
    }

    public void getBook(long id) {
        Future<Book> fbooks = databaseWriteExecutor.submit(() -> bookDao.getBook(id));
        try {
            selectedBook.setValue(fbooks.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public MutableLiveData<Book> getSelectedBook() {
        return selectedBook;
    }

    public LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }
}
