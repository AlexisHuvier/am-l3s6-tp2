package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;


import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.BookRepository;

public class DetailViewModel extends AndroidViewModel {
    private BookRepository repository;
    private MutableLiveData<Book> book;

    public DetailViewModel(Application application) {
        super(application);
        repository = new BookRepository(application);
        book = repository.getSelectedBook();
    }

    MutableLiveData<Book> getBook() {
        return book;
    }

    public void updateorInsertBook(String title, String authors, String year, String genres, String publisher) {
        if(book != null) {
            Book internalBook = book.getValue();
            internalBook.setTitle(title);
            internalBook.setAuthors(authors);
            internalBook.setYear(year);
            internalBook.setGenres(genres);
            internalBook.setPublisher(publisher);
            book.setValue(internalBook);
            repository.updateBook(internalBook);
        }
        else {
            Book internalBook = new Book(title, authors, year, genres, publisher);
            book = new MutableLiveData<>(internalBook);
            repository.insertBook(internalBook);
        }
    }

    public void setBook(long id) {
        if(id == -1)
            book = null;
        else {
            repository.getBook(id);
            book = repository.getSelectedBook();
        }
    }
}
