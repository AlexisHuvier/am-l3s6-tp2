package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailFragment extends Fragment {
    private DetailViewModel viewModel;
    private EditText textTitle, textAuthors, textYear, textGenres, textPublisher;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        // Get selected book
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        viewModel.setBook(args.getBookNum());

        textTitle = (EditText) view.findViewById(R.id.nameBook);
        textAuthors = (EditText) view.findViewById(R.id.editAuthors);
        textYear = (EditText) view.findViewById(R.id.editYear);
        textGenres = (EditText) view.findViewById(R.id.editGenres);
        textPublisher = (EditText) view.findViewById(R.id.editPublisher);

        if(viewModel.getBook() != null) {
            Book book = viewModel.getBook().getValue();

            textTitle.setText(book.getTitle());
            textAuthors.setText(book.getAuthors());
            textYear.setText(book.getYear());
            textGenres.setText(book.getGenres());
            textPublisher.setText(book.getPublisher());
        }

        view.findViewById(R.id.buttonBack).setOnClickListener(view1 -> NavHostFragment.findNavController(DetailFragment.this)
                .navigate(R.id.action_SecondFragment_to_FirstFragment));

        view.findViewById(R.id.buttonUpdate).setOnClickListener(view1 -> {
            String title = textTitle.getText().toString();
            String authors = textAuthors.getText().toString();
            String year = textYear.getText().toString();
            String genres = textGenres.getText().toString();
            String publisher = textPublisher.getText().toString();
            if(!title.isEmpty() && !authors.isEmpty() && !year.isEmpty() && !genres.isEmpty() && !publisher.isEmpty())
                viewModel.updateorInsertBook(title, authors, year, genres, publisher);
            else
                Snackbar.make(view, "Un des champs n'a pas été remplit", Snackbar.LENGTH_LONG).show();
        });
    }
}